import React, { useEffect, useState } from 'react'
import Form from './component/Form'



function App() {
  const [backendData, setBackenData] = useState([{}])

  useEffect(() => {
    fetch("/").then(
      response => response.json()
    ).then(
      data => {
        setBackenData(data)
      }
    )
  }, [])
  console.log(backendData);
  return (
    <div>
      {(typeof backendData.users === 'undefined') ? (
        <p>Loading ...</p>
      ) : (
        backendData.users.map((user, i) => (
          <p key={i}>{user}</p>
        ))
      )}
      <div>
        <Form />
      </div>
    </div>
  )
}

export default App